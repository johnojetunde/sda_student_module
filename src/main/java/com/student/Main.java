package com.student;

import com.student.entity.*;
import com.student.util.HibernateUtil;
import org.hibernate.Session;

import java.time.LocalDate;

import static java.util.Arrays.asList;

public class Main {
    public static void main(String[] args) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        /**
         * We always need to set the values from the parent; This is because,
         * it is easier for hibernate to do a cascade from the parent to the child
         *
         * The parent in a Many-One/One-Many relation is the entity that has has @mappedBy and the child is entity that has @JoinColumn
         * */

        /**Attendance has no child; It is rather a child to Person(student) and to Module(module)*/
        Attendance attend1 = new Attendance(LocalDate.now(), false);
        Attendance attend2 = new Attendance(LocalDate.now(), false);
        Attendance attend3 = new Attendance(LocalDate.now(), true);

        /**Module is a parent of Attendance(attendance), but it is also a child of Team(team), Topic(topic), Person(trainer) and Classroom(classroom)*/
        Module module = new Module()
                .setStartDate(LocalDate.now())
                .setEndDate(LocalDate.now())
                .addAttendance(attend1)
                .addAttendance(attend3);

        Module trainerModule = new Module()
                .setStartDate(LocalDate.now())
                .setEndDate(LocalDate.now())
                .addAttendance(attend2);

        /**Topic has one single child i.e Module(module)*/
        Topic topic1 = new Topic("Java Fundamentals").addModule(module);
        Topic topic2 = new Topic("JDBC Hibernate").addModule(trainerModule);
        Topic topic3 = new Topic("Design Patterns")
                .addModule(module)
                .addModule(trainerModule);

        /**Person is a parent of Attendances(attendances i.e students are the only one that should have attendance)
         * and Person(trainer i.e trainers are the only one that should have modules)
         * Person is also a child of Team
         * */
        Person person = initializePerson("email1", true)
                .addModule(trainerModule);
        Person person3 = initializePerson("email3", true)
                .addModule(trainerModule);
        Person person2 = initializePerson("email2", false)
                .addAttendance(attend1);
        Person person4 = initializePerson("email4", false)
                .addAttendance(attend2);

        /**
         * Classroom has a child (Module). Classroom does not belong to any parent
         * */
        Classroom room1 = new Classroom("LightHouse", "LightHouse")
                .addModule(trainerModule);
        Classroom room2 = new Classroom("LightHouse2", "LightHouse2")
                .addModule(module);
        Classroom room3 = new Classroom("LightHouse3", "LightHouse3")
                .addModule(module);

        /**
         * Team has a child (Person and Module). Team does not belong to any parent
         * */
        Team team1 = new Team("SDA")
                .addPerson(person)
                .addPerson(person3)
                .addModule(trainerModule);

        Team team2 = new Team("Codecademy")
                .addPerson(person2)
                .addPerson(person4)
                .addModule(module)
                .addModule(trainerModule);

        saveAll(session, asList(topic1, topic2, topic3));
        saveAll(session, asList(team1, team2));
        saveAll(session, asList(attend1, attend2));
        saveAll(session, asList(module, trainerModule));
        saveAll(session, asList(person, person2, person3, person4));
        saveAll(session, asList(room1, room2, room3));

        // committing
        session.getTransaction().commit();

        Person person1 = session.find(Person.class, 1L);
        Team updatedTeam = session.find(Team.class, 1L);
        updatedTeam.getPersons().forEach(System.out::println);

        HibernateUtil.shutdown();

    }

    private static Person initializePerson(String email, boolean trainer) {
        return new Person()
                .setFirstName("firstname")
                .setLastName("lastName")
                .setEmail(email)
                .setDateOfBirth(LocalDate.now())
                .setTrainer(trainer);
    }

    private static <T> void saveAll(Session session, Iterable<T> entities) {
        entities.forEach(session::saveOrUpdate);
    }
}
