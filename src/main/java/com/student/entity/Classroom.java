package com.student.entity;

import lombok.Data;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "classroom")
public class Classroom {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NaturalId
    private String name;
    private String address;
    @OneToMany(mappedBy = "classroom")
    private Set<Module> modules;

    public Classroom(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public Classroom addModule(Module module) {
        if (modules == null) {
            modules = new HashSet<>();
        }
        modules.add(module);
        return this;
    }
}
