package com.student.entity;

import lombok.Data;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "topic")
public class Topic {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NaturalId
    private String name;

    @OneToMany(mappedBy = "topic")
    private Set<Module> modules;

    public Topic(String name) {
        this.name = name;
    }

    public Topic addModule(Module module) {
        if (modules == null) {
            modules = new HashSet<>();
        }
        modules.add(module);
        return this;
    }
}
