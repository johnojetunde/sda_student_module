package com.student.entity;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@ToString
@Data
@Entity
@Accessors(chain = true)
@Table(name = "person")
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    @NaturalId
    private String email;
    private LocalDate dateOfBirth;
    private boolean isTrainer;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "team_id")
    private Team team;
    @OneToMany(mappedBy = "student")
    private Set<Attendance> attendances;
    @OneToMany(mappedBy = "trainer")
    private Set<Module> modules;

    public Person addModule(Module module) {
        if (modules == null) {
            modules = new HashSet<>();
        }
        modules.add(module);
        return this;
    }

    public Person addAttendance(Attendance attendance) {
        if (attendances == null) {
            attendances = new HashSet<>();
        }
        attendances.add(attendance);
        return this;
    }
}
