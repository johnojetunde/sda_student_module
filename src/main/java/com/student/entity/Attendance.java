package com.student.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "attendance")
public class Attendance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDate date;
    private boolean isPresent;
    @ManyToOne
    @JoinColumn(name = "module_id")
    private Module module;
    @ManyToOne
    @JoinColumn(name = "student_id")
    private Person student;

    public Attendance(LocalDate date, boolean present) {
        this.date = date;
        this.isPresent = present;
    }
}
